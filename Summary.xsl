<?xml version="1.0" ?> 
	<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>
		<xsl:template match="/">
			<html>
				<body>
					<h2>Recent Blog Entries at http://gotoknow.org</h2> 
						<table border="1">
							<tr bgcolor="#9acd32">
							<th align="center">Title</th> 
							<th align="center">Author</th> 
							<th align="center">Link</th>
						</tr>
							<xsl:for-each select="//item">
							<tr>
						<td>
						  <xsl:value-of select="title" /> 
						  </td>
						<td>
						  <xsl:value-of select="author"/> 
						  </td>
						<td>
						<a>
									<xsl:attribute name="href">
										<xsl:value-of select="link"/> 
									</xsl:attribute>
										<xsl:value-of select="link"/> 
								</a>						 
						</td>
						  </tr>
						  </xsl:for-each>
						</table>
				</body>
			</html>
		</xsl:template>
  </xsl:stylesheet>