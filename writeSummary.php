<?php
	
        $doc = new DOMDocument;
        $dom = new DomDocument('1.0','UTF-8'); 
        $doc->load('http://www.gotoknow.org/blogs/posts?format=rss');
		
		 $root = $doc->documentElement;
        $elms = $root->getElementsByTagName('item');

        echo "Reading from summaryBlogs.xml<br>";
        $items = $dom->appendChild($dom->createElement('items'));

        foreach ($elms AS $item) {
                $title1 = $item->getElementsByTagName("title")->item(0)->nodeValue;
                $link1 = $item->getElementsByTagName("link")->item(0)->nodeValue;
                $author1 = $item->getElementsByTagName("author")->item(0)->nodeValue;
                
                $item = $items->appendChild($dom->createElement('item')); 

                $title = $item->appendChild($dom->createElement('title')); 
                $title->appendChild( $dom->createTextNode($title1));
				
                $link = $item->appendChild($dom->createElement('link'));
                $link->appendChild( $dom->createTextNode($link1));
				
                $author = $item->appendChild($dom->createElement('author')); 
                $author->appendChild( $dom->createTextNode($author1));

                echo $link1 . $author1;
        }

        $dom->formatOutput = true;
        $test1 = $dom->saveXML();
        $dom->save('summaryBlogs.xml'); 
?>